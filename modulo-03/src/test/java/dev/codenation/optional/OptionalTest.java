package dev.codenation.optional;

import org.junit.Test;

public class OptionalTest {


    @Test
    public void teste2() {
        Caminhao caminhao = new Caminhao();

        Pessoa locatario = new Pessoa();

        Empresa empresa = new Empresa();
        empresa.setNome("Empresa");

        locatario.setEmpresa(empresa);
        caminhao.setLocatario(locatario);

        Pessoa proprietario = new Pessoa();
        caminhao.setProprietario(proprietario);

        System.out.println(caminhao.buscarEmpresaLocatario());
    }

    @Test
    public void teste3() {
        Caminhao caminhao = new Caminhao();
        caminhao.setLocatario(new Pessoa());
        String empresaLocatario = caminhao.buscarEmpresaLocatario();

        System.out.println(empresaLocatario);
    }

}
