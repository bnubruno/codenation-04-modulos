package dev.codenation.empresa;

import org.hamcrest.Matchers;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class EmpresaTest {

    private static final String NOME = "Bruno";
    private static final Integer NUMERO_MATRICULA = 123;
    private static final String NOTURNO = "Noturno";
    private static final BigDecimal SALARIO_BASE = new BigDecimal("2500");
    private static final BigDecimal ADICIONAL_NOTURNO = new BigDecimal("400");
    private static final String EXIBE_DADOS_ADMINISTRATIVO = "turno='Noturno', adicionalNoturno=400, nome='Bruno', numeroMatricula='123', salarioBase=2500";

    @Test
    public void quandoForAdministrativoRetornaExibeDados() {
        Administrativo administrativo = new Administrativo();
        administrativo.setAdicionalNoturno(ADICIONAL_NOTURNO);
        administrativo.setNome(NOME);
        administrativo.setNumeroMatricula(NUMERO_MATRICULA);
        administrativo.setSalarioBase(SALARIO_BASE);
        administrativo.setTurno(NOTURNO);

        String result = administrativo.exibeDados();

        assertThat(result, equalTo(EXIBE_DADOS_ADMINISTRATIVO));
        assertThat(administrativo.getNome(), Matchers.equalTo(NOME));
    }

}
