package dev.codenation.main;

import dev.codenation.animal.Cachorro;
import dev.codenation.animal.Gato;
import dev.codenation.empresa.Administrativo;
import dev.codenation.empresa.Funcionario;
import dev.codenation.empresa.Tecnico;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Main {

    /**
     * A. crie a classe Assistente, que também é um funcionário, e que possui um número de
     * matrícula (faça o método GET). Sobrescreva o método exibeDados().
     * B. sabendo que os Assistentes Técnicos possuem um bônus salarial e que os
     * Assistentes Administrativos possuem um turno (dia ou noite) e um adicional
     * noturno, crie as classes Tecnico e Administrativo.
     * <p>
     * classes: Assistente, Funcionário, Tecnico, Administrativo
     * <p>
     * métodos exibeDados: String concatenando atributos
     * <p>
     * atributos: numeroMatricula, bonusSalarial, adicionalNuturno, turno
     *
     * @param args
     */


    public static void main(String[] args) {
        Funcionario funcionario = new Funcionario();
        funcionario.setNome("Eduardo");
        funcionario.setNumeroMatricula(123456);
        funcionario.setSalarioBase(BigDecimal.valueOf(800));

        Administrativo administrativo = new Administrativo();
        administrativo.setNome("Eduardo");
        administrativo.setNumeroMatricula(123456);
        administrativo.setSalarioBase(BigDecimal.valueOf(800));
        administrativo.setTurno("Noturno");
        administrativo.setAdicionalNoturno(new BigDecimal("400"));

        Tecnico tecnico = new Tecnico();
        tecnico.setNome("Eduardo");
        tecnico.setNumeroMatricula(123456);
        tecnico.setSalarioBase(BigDecimal.valueOf(800));
        tecnico.setBonusSalarial(new BigDecimal(400));

        System.out.println(tecnico.exibeDados());
    }


}
