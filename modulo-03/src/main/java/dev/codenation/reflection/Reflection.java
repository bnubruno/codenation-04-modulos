package dev.codenation.reflection;

import dev.codenation.optional.Caminhao;
import dev.codenation.optional.MeuCampo;
import dev.codenation.optional.Pessoa;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Reflection {

    public static void main(String[] args) {
        Reflection reflection = new Reflection();
        Caminhao caminhao =
                Caminhao.builder()
                .locatario(new Pessoa())
                .proprietario(new Pessoa())
                .build();

        caminhao.setValor(new BigDecimal("400"));
        caminhao.setOutroValor("abc");

        BigDecimal valor = reflection.buscarValorAtributo("valor", caminhao, caminhao.getClass());
        System.out.println(valor);

        BigDecimal locatario = reflection.buscarValorAtributo("locatario", caminhao, caminhao.getClass());
        System.out.println(locatario);

        reflection.buscarAtributosDaClasseMeuCampo(caminhao.getClass()).forEach(field -> System.out.println(field));

        String outroValor = reflection.buscarValorAtributo("outroValor", caminhao, caminhao.getClass());
        System.out.println(outroValor);

        System.out.println(reflection.buscarNomeClasse(caminhao.getClass()));
        reflection.buscarAtributosDaClasse(caminhao.getClass()).forEach(field -> System.out.println(field));
    }


    public String buscarNomeClasse(Class clazz) {
        return clazz.getCanonicalName();
    }

    public List<String> buscarAtributosDaClasse(Class clazz) {
        return Arrays.asList(clazz.getDeclaredFields()).stream().map(field -> field.getName()).collect(Collectors.toList());
    }

    public List<String> buscarAtributosDaClasseMeuCampo(Class clazz) {
        return Arrays.asList(clazz.getDeclaredFields()).stream()
                .filter(field -> {
                    MeuCampo annotation = field.getAnnotation(MeuCampo.class);
                    if (annotation != null) {
                        System.out.println(annotation.value());
                    }
                    return annotation != null;
                })
                .map(field -> field.getName()).collect(Collectors.toList());
    }

    public <T> T buscarValorAtributo(String nomeAtributo, Object obj, Class clazz) {
        return Arrays.asList(clazz.getDeclaredFields()).stream()
                .filter(field -> field.getName().equals(nomeAtributo))
                .map(field -> {
                    try {
                        field.setAccessible(true);
                        return (T) field.get(obj);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .filter(field -> field != null)
                .findFirst()
                .orElse(null);
    }


}
