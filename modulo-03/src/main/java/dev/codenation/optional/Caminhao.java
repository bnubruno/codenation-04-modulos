package dev.codenation.optional;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Optional;

@Data
@Builder
public class Caminhao {

    private Pessoa proprietario;
    @MeuCampo("A")
    private Pessoa locatario;
    @MeuCampo("B")
    private BigDecimal valor;
    private String outroValor;

    public Optional<Pessoa> getLocatario() {
        return Optional.ofNullable(locatario);
    }

    public Optional<Pessoa> getProprietario() {
        return Optional.ofNullable(proprietario);
    }

    public String buscarEmpresaLocatario() {
        return getLocatario()
                .flatMap(locatario -> locatario.getEmpresa())
                .map(empresa -> empresa.getNome())
                .orElseGet(() -> buscarNaReceitaFederal());
    }

    private String buscarNaReceitaFederal() {
        return "o que eu quiser";
    }

}
