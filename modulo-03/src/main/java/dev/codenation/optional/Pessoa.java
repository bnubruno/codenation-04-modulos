package dev.codenation.optional;

import lombok.Data;

import java.util.Optional;

@Data
public class Pessoa {

    private String nome;
    private Empresa empresa;

    public Optional<Empresa> getEmpresa() {
        return Optional.ofNullable(empresa);
    }
}
