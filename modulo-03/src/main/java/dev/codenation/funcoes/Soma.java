package dev.codenation.funcoes;

import java.math.BigDecimal;
import java.util.function.BiFunction;

public class Soma {

    public BigDecimal soma(BigDecimal a, BigDecimal b, BiFunction<BigDecimal, BigDecimal, BigDecimal> funcao) {
        return funcao.apply(a, b);
    }

    public static void main(String[] args) {
        new Soma().soma(new BigDecimal("30"), new BigDecimal("15"), (a, b) -> {
            return a.add(b);
        });
    }

}
