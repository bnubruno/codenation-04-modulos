package dev.codenation.funcoes;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Funcao {

    public static void main(String[] args) {
        List<String> listasDeFrutas = new ArrayList<>();
        listasDeFrutas.add("jabuticaba");
        listasDeFrutas.add("uva");
        listasDeFrutas.add("jabuticaba");
        listasDeFrutas.add("banana");

//        List<Integer> listaDeLength = new ArrayList<>();
//        listaDeLength.add("jabuticaba".length());
//        listaDeLength.add("uva".length());
//        listaDeLength.add("jabuticaba".length());

        List<String> listaSoDeJabuticaba = listasDeFrutas.stream()
                .filter(fruta -> {
                    return fruta.equals("jabuticaba");
                })
                .collect(Collectors.toList());

//        listaSoDeJabuticaba.forEach(fruta -> {
//            System.out.println(fruta);
//        });
        System.out.println("-- Foreach");
        listasDeFrutas.forEach(fruta -> System.out.println(fruta));
        String first = listasDeFrutas.stream().findFirst().get();
        long count = listasDeFrutas.stream().count();
        System.out.println("-- Map");
        List<Integer> listaDeLength = listasDeFrutas.stream().map(x -> x.length()).collect(Collectors.toList());
        listaDeLength.forEach(fruta -> System.out.println(fruta));
        System.out.println("Firt: " + first);
        System.out.println("Quantidade: " + count);
        List<String> listaDeFrutasOrdenadas = listasDeFrutas.stream().sorted().collect(Collectors.toList());
        System.out.println("-- Ordenado");
        listaDeFrutasOrdenadas.forEach(fruta -> System.out.println(fruta));
    }


}
