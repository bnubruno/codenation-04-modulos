package dev.codenation.empresa;

import java.math.BigDecimal;

public class Tecnico extends Assistente {

    private BigDecimal bonusSalarial;


    @Override
    public String exibeDados() {
        return "bonusSalarial='" + bonusSalarial + '\'' +
                ", " + super.exibeDados();
    }

    public BigDecimal getBonusSalarial() {
        return bonusSalarial;
    }

    public void setBonusSalarial(BigDecimal bonusSalarial) {
        this.bonusSalarial = bonusSalarial;
    }
}
