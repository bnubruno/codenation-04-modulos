package dev.codenation.empresa;

import java.math.BigDecimal;

public class Funcionario {

    private String nome;
    private Integer numeroMatricula;
    private BigDecimal salarioBase;

    public String exibeDados() {
        return "nome='" + nome + '\'' +
                ", numeroMatricula='" + numeroMatricula + '\'' +
                ", salarioBase=" + salarioBase;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getNumeroMatricula() {
        return numeroMatricula;
    }

    public void setNumeroMatricula(Integer numeroMatricula) {
        this.numeroMatricula = numeroMatricula;
    }

    public BigDecimal getSalarioBase() {
        return salarioBase;
    }

    public void setSalarioBase(BigDecimal salarioBase) {
        this.salarioBase = salarioBase;
    }
}
