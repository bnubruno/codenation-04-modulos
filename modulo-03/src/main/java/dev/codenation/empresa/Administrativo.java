package dev.codenation.empresa;

import java.math.BigDecimal;

public class Administrativo extends Assistente {

    private String turno;
    private BigDecimal adicionalNoturno;

    @Override
    public String exibeDados() {
        return "turno='" + turno + '\'' +
                ", adicionalNoturno=" + adicionalNoturno
                + ", " + super.exibeDados();
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public BigDecimal getAdicionalNoturno() {
        return adicionalNoturno;
    }

    public void setAdicionalNoturno(BigDecimal adicionalNoturno) {
        this.adicionalNoturno = adicionalNoturno;
    }
}
