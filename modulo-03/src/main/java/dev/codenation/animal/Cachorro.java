package dev.codenation.animal;

import java.util.List;

public class Cachorro extends Estimacao {

    public Cachorro(String especie, String tipoReproducao, String classificacao, String alimentacao, String nome, List<String> vacinas) {
        super(especie, tipoReproducao, classificacao, alimentacao, nome, vacinas);
    }

    public void late() {
        System.out.println("Late");
    }

    @Override
    public String alimenta() {
        return super.alimenta() + " para cachorros";
    }
}
