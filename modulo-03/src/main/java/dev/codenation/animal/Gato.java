package dev.codenation.animal;

import java.util.List;

public class Gato extends Estimacao {

    public Gato(String especie, String tipoReproducao, String classificacao, String alimentacao, String nome, List<String> vacinas) {
        super(especie, tipoReproducao, classificacao, alimentacao, nome, vacinas);
    }

    public void mia() {
        System.out.println("Mia");
    }

    @Override
    public String alimenta() {
        return super.alimenta() + " para gatos";
    }
}
