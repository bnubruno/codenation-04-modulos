package dev.codenation.animal;

import java.util.List;

// alt + enter
public class Estimacao extends Animal {

    private String nome;
    private List<String> vacinas;

    public Estimacao(String especie, String tipoReproducao, String classificacao, String alimentacao, String nome, List<String> vacinas) {
        super(especie, tipoReproducao, classificacao, alimentacao);
        this.nome = nome;
        this.vacinas = vacinas;
    }

    @Override
    public String alimenta() {
        return "Se alimentam: Ração";
    }

    public String brinca() {
        return "Brinca";
    }

    public String treina() {
        return "Treina";
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<String> getVacinas() {
        return vacinas;
    }

    public void setVacinas(List<String> vacinas) {
        this.vacinas = vacinas;
    }
}
