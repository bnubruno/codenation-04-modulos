package dev.codenation.animal;

public class Animal {

    private String especie;
    private String tipoReproducao;
    private String classificacao;
    private String alimentacao;

    public Animal(String especie, String tipoReproducao, String classificacao, String alimentacao) {
        this.especie = especie;
        this.tipoReproducao = tipoReproducao;
        this.classificacao = classificacao;
        this.alimentacao = alimentacao;
    }

    public String alimenta() {
        return "Se alimentam com: Frutas da floresta, animais e plantas, gordudas e açúcares";
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getTipoReproducao() {
        return tipoReproducao;
    }

    public void setTipoReproducao(String tipoReproducao) {
        this.tipoReproducao = tipoReproducao;
    }

    public String getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(String classificacao) {
        this.classificacao = classificacao;
    }

    public String getAlimentacao() {
        return alimentacao;
    }

    public void setAlimentacao(String alimentacao) {
        this.alimentacao = alimentacao;
    }
}
