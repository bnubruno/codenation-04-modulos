package dev.codenation.modulo05.repositorios;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PedidoRepositoryTest {

    @Autowired
    public PedidoRepository pedidoRepository;

    @Test
    public void buscaPedidoPorNomeClienteQuandoTemChaveEmbbeded() {
        pedidoRepository.findByIdClienteNome("Bruno");
    }

}
