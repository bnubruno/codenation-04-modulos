package dev.codenation.modulo05.repositorios;

import dev.codenation.modulo05.entidades.Cargo;
import dev.codenation.modulo05.entidades.Empresa;
import dev.codenation.modulo05.entidades.Funcionario;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

@SpringBootTest
public class EmpresaRepositoryTest {

    @Autowired
    private EmpresaRepository empresaRepository;

    @Autowired
    private CargoRepository cargoRepository;

    @Autowired
    private FuncionarioRepository funcionarioRepository;


    @Test
    @Transactional
    public void quandoBuscarTodos() {
        Empresa empresa2 = criarEmpresa("Empresa Teste");

        List<Empresa> result = empresaRepository.findAll();


        assertThat(result, hasSize(1));
    }

    @Test
    @Transactional
    public void quandoBuscarPorIdQueNaoExisteRetornarVazio() {
        Empresa empresa2 = criarEmpresa("Empresa Teste");

        Optional<Empresa> result = empresaRepository.findById(UUID.randomUUID());

        assertThat(result.isPresent(), is(Boolean.FALSE));
    }

    @Test
    @Transactional
    public void quandoBuscarPorIdQueExiste() {
        Empresa empresa = criarEmpresa("Empresa Teste");

        Optional<Empresa> result = empresaRepository.findById(empresa.getId());

        assertThat(result.isPresent(), is(Boolean.TRUE));
        assertThat(result.get().getNome(), equalTo("Empresa Teste"));
    }

    @Test
    @Transactional
    public void quandoBuscarUmaEmpresaPorFuncionarioNomeEMatricula() {
        Cargo cargo = criarCargo();
        Empresa empresa = criarEmpresa("Empresa Teste");
        criarEmpresa("Empresa Sem Funcionario");
        criarFuncionario(cargo, empresa);

        List<Empresa> empresas = empresaRepository.findByFuncionariosNomeAndFuncionariosMatricula("Bruno", "123456");

        assertThat(empresas, hasSize(1));
        assertThat(empresas.get(0).getNome(), equalTo("Empresa Teste"));
    }

    @Test
    @Transactional
    public void quandoBuscaEmpresaPeloNome() {
        Empresa empresa = criarEmpresa("Empresa Teste");

        List<Empresa> empresas = empresaRepository.findByNome("Empresa Teste");

        assertThat(empresas, hasSize(1));
        assertThat(empresas.get(0).getNome(), equalTo("Empresa Teste"));
    }

    @Test
    @Transactional
    public void quandoBuscarUmaEmpresaPorFuncionariosDeUmCargo() {
        Cargo cargo = criarCargo();
        Empresa empresa = criarEmpresa("Empresa Teste");
        criarEmpresa("Empresa Sem Funcionario");
        criarFuncionario(cargo, empresa);

        List<Empresa> empresas = empresaRepository.findByFuncionariosCargoDescricao("Analista de Sistemas");

        assertThat(empresas, hasSize(1));
        assertThat(empresas.get(0).getNome(), equalTo("Empresa Teste"));
    }

    @Test
    @Transactional
    public void quandoBuscarUmaEmpresaPorFuncionariosDeUmCargoENomeDaEmpresa() {
        Cargo cargo = criarCargo();
        Empresa empresa = criarEmpresa("Empresa Teste");
        criarEmpresa("Empresa Sem Funcionario");
        criarFuncionario(cargo, empresa);

        List<Empresa> empresas = empresaRepository.findByNomeAndFuncionariosCargoDescricao("Empresa Teste", "Analista de Sistemas");

        assertThat(empresas, hasSize(1));
        assertThat(empresas.get(0).getNome(), equalTo("Empresa Teste"));
    }

    private Cargo criarCargo() {
        Cargo cargo = Cargo.builder()
                .descricao("Analista de Sistemas")
                .build();
        cargoRepository.save(cargo);
        return cargo;
    }

    private Empresa criarEmpresa(String s) {
        Empresa empresa = Empresa.builder()
                .nome(s)
                .razaoSocial("Empresa Teste de Ser")
                .build();
        empresaRepository.save(empresa);
        return empresa;
    }

    private Funcionario criarFuncionario(Cargo cargo, Empresa empresa) {
        return funcionarioRepository.save(Funcionario.builder()
                .cargo(cargo)
                .dataAdmissao(LocalDate.now())
                .matricula("123456")
                .nome("Bruno")
                .salario(new BigDecimal("2000"))
                .empresa(empresa)
                .build());
    }


}
