package dev.codenation.modulo05.entidades;

import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;

import java.util.Set;

import static javax.validation.Validation.buildDefaultValidatorFactory;

public class CargoTest {

    @Test
    public void abc() {
        Cargo cargo = Cargo.builder()
                .descricao("1233123123123")
                .build();

        Set<ConstraintViolation<Cargo>> validacoes = buildDefaultValidatorFactory().getValidator().validate(cargo);
        validacoes.forEach(cv -> System.out.println(cv.getPropertyPath() + " " + cv.getMessage()));
    }


}
