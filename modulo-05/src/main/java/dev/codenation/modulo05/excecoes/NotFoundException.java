package dev.codenation.modulo05.excecoes;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NO_CONTENT)
public class NotFoundException extends Exception {

    public NotFoundException(String message) {
        super(message);
    }
}
