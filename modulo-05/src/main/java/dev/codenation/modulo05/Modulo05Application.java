package dev.codenation.modulo05;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class Modulo05Application {

	public static void main(String[] args) {
		SpringApplication.run(Modulo05Application.class, args);
	}

}
