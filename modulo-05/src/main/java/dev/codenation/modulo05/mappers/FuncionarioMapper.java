package dev.codenation.modulo05.mappers;

import dev.codenation.modulo05.dtos.CargoDTO;
import dev.codenation.modulo05.dtos.EmpresaDTO;
import dev.codenation.modulo05.dtos.FuncionarioDTO;
import dev.codenation.modulo05.entidades.Funcionario;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.math.BigDecimal;
import java.util.UUID;

@Mapper(componentModel = "spring", uses = {CargoMapper.class, EmpresaMapper.class})
public interface FuncionarioMapper {

    @Mapping(target = "cargo", ignore = true)
    FuncionarioDTO mapSemCargo(Funcionario dto);

    FuncionarioDTO mapComCargo(Funcionario dto);

}
