package dev.codenation.modulo05.mappers;

import dev.codenation.modulo05.dtos.EmpresaDTO;
import dev.codenation.modulo05.entidades.Empresa;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EmpresaMapper {

    EmpresaDTO map(Empresa empresa);

}
