package dev.codenation.modulo05.mappers;

import dev.codenation.modulo05.dtos.CargoDTO;
import dev.codenation.modulo05.entidades.Cargo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;
import java.util.UUID;

@Mapper(componentModel = "spring")
public interface CargoMapper {

    @Mapping(source = "id", target = "id")
    @Mapping(source = "dto.minha_descricao", target = "descricao")
    Cargo paraCargo(CargoDTO dto, UUID id);

    @Mapping(source = "descricao", target = "minha_descricao")
    CargoDTO paraCargoDTO(Cargo dto);

    List<CargoDTO> paraCargoDTO(List<Cargo> cargos);

    List<Cargo> paraCargo(List<CargoDTO> cargos);

}
