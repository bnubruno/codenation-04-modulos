package dev.codenation.modulo05.services;

import dev.codenation.modulo05.entidades.Cargo;
import dev.codenation.modulo05.repositorios.CargoRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class CargoService {

    private CargoRepository cargoRepository;

    public Cargo salvar(@Valid Cargo cargo) {
        return cargoRepository.save(cargo);
    }

    public Optional<Cargo> buscarPorId(UUID id) {
        return cargoRepository.findById(id);
    }

    public Page<Cargo> buscarTodos(Pageable pageable) {
        return cargoRepository.findAll(pageable);
    }
}
