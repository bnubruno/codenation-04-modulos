package dev.codenation.modulo05.services;

import dev.codenation.modulo05.entidades.Empresa;
import dev.codenation.modulo05.repositorios.EmpresaRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class EmpresaService {

    private EmpresaRepository empresaRepository;

    public List<Empresa> buscarEmpresasPorNomeFuncionario(String nomeFuncionario) {
        return empresaRepository.buscarEmpresasPorNomeFuncionario(nomeFuncionario);
    }

    public List<Empresa> buscarPorNomeFuncionario(String nome) {
        return empresaRepository.findByFuncionariosNome(nome);
    }

    public List<Empresa> buscarPorNomeEMatricula(String nome, String matricula) {
        return empresaRepository.findByFuncionariosNomeAndFuncionariosMatricula(nome, matricula);
    }

}
