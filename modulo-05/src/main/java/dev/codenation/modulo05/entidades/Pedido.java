package dev.codenation.modulo05.entidades;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Pedido extends AbstractAuditingEntity {

    @EmbeddedId
    private PedidoId id;

    private LocalDateTime data;
    private BigDecimal valor;

}
