package dev.codenation.modulo05.entidades;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class PedidoId implements Serializable {

    @ManyToOne
    private Empresa empresa;

    @ManyToOne
    private Cliente cliente;

}
