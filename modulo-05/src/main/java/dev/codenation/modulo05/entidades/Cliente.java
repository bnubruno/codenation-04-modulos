package dev.codenation.modulo05.entidades;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Cliente extends AbstractAuditingEntity {

    @Id
    private UUID id;

    @NotNull
    @Size(max = 60)
    private String nome;

    @OneToMany(mappedBy = "id.cliente")
    private List<Pedido> pedidos;

}
