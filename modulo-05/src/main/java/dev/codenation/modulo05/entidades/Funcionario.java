package dev.codenation.modulo05.entidades;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Funcionario extends AbstractAuditingEntity {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private UUID id;

    @NotNull
    @Size(max = 60)
    private String nome;

    @NotNull
    @Size(max = 10)
    private String matricula;

    @NotNull
    @Digits(integer = 13, fraction = 2)
    private BigDecimal salario;

    @PastOrPresent
    @NotNull
    private LocalDate dataAdmissao;

    @ManyToOne
    private Empresa empresa;

    @ManyToOne
    private Cargo cargo;


}
