package dev.codenation.modulo05.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RespostaDTO {

    private Long id;
    private String nome;
    private Integer idade;

}
