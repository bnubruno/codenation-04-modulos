package dev.codenation.modulo05.dtos;

import dev.codenation.modulo05.entidades.Cargo;
import dev.codenation.modulo05.entidades.Empresa;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FuncionarioDTO {

    private UUID id;
    private String nome;
    private String matricula;
    private BigDecimal salario;
    private String dataAdmissao;
    private EmpresaDTO empresa;
    private CargoDTO cargo;

}
