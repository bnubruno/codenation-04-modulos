package dev.codenation.modulo05.controller;

import dev.codenation.modulo05.dtos.ErroDTO;
import dev.codenation.modulo05.excecoes.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionController {

    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    public ErroDTO handleNotFoundException(NotFoundException exception) {
        return ErroDTO.builder()
                .message(exception.getMessage())
                .code(HttpStatus.NOT_FOUND.value())
                .timestamp(System.currentTimeMillis())
                .build();
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErroDTO handleConstraintValidationException(MethodArgumentNotValidException exception) {
        return ErroDTO.builder()
                .message(getMessages(exception))
                .code(HttpStatus.NOT_FOUND.value())
                .timestamp(System.currentTimeMillis())
                .build();
    }

    private List<String> getMessages(MethodArgumentNotValidException exception) {
        return exception.getBindingResult().getFieldErrors()
                .stream().map(fe -> fe.getField() + ": " + fe.getDefaultMessage())
                .collect(Collectors.toList());
    }


}
