package dev.codenation.modulo05.controller;

import dev.codenation.modulo05.dtos.FuncionarioDTO;
import dev.codenation.modulo05.entidades.Cargo;
import dev.codenation.modulo05.entidades.Empresa;
import dev.codenation.modulo05.entidades.Funcionario;
import dev.codenation.modulo05.mappers.FuncionarioMapper;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.UUID;

@RestController
@AllArgsConstructor
public class FuncionarioController {

    private FuncionarioMapper funcionarioMapper;

    @GetMapping("/funcionarios")
    public FuncionarioDTO findById() {
        Funcionario funcionario = Funcionario.builder()
                .id(UUID.randomUUID())
                .empresa(Empresa.builder()
                        .id(UUID.randomUUID())
                        .build())
                .cargo(Cargo.builder()
                        .descricao("Analista de Sistemas")
                        .build())
                .dataAdmissao(LocalDate.now())
                .build();
        return funcionarioMapper.mapSemCargo(funcionario);
    }


}
