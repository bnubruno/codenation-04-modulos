package dev.codenation.modulo05.controller;

import dev.codenation.modulo05.dtos.RespostaDTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/respostas")
public class RespostaController {

    private List<RespostaDTO> respostaDTOS;

    @GetMapping
    public List<RespostaDTO> buscarTodas(@RequestParam(value = "nome", required = false) Optional<String> nome,
                                         @RequestParam(value = "idade", required = false) Optional<Integer> idade) {
        return nome.map(this::getRespostasByNome)
                .orElseGet(() -> idade.map(this::getRespostasByIdade)
                        .orElseGet(() -> getRespostaDTOS()));
    }

    @GetMapping("/{id}")
    public RespostaDTO buscarPorId(@PathVariable("id") Long id) {
        return getRespostasById(id).orElse(null);
    }

    @PutMapping("/{id}")
    public RespostaDTO editar(@PathVariable("id") Long id, @RequestBody RespostaDTO respostaDTO) {
        RespostaDTO respostaDTOSalva = getRespostasById(id).orElseThrow(() -> new NullPointerException("Resposta não existe"));
        respostaDTOSalva.setIdade(respostaDTO.getIdade());
        respostaDTOSalva.setNome(respostaDTO.getNome() != null ? respostaDTO.getNome() : respostaDTOSalva.getNome());
        return respostaDTO;
    }

    private Optional<RespostaDTO> getRespostasById(@PathVariable("id") Long id) {
        return getRespostaDTOS().stream()
                .filter(r -> r.getId().equals(id))
                .findFirst();

    }

    @PostMapping
    public RespostaDTO salvar(@RequestBody RespostaDTO respostaDTO) {
        getRespostaDTOS().add(respostaDTO);
        return respostaDTO;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        respostaDTOS = getRespostaDTOS().stream().filter(r -> !r.getId().equals(id)).collect(Collectors.toList());
        System.out.println(respostaDTOS.size());
    }

    private List<RespostaDTO> getRespostasByNome(String nome) {
        return getRespostaDTOS()
                .stream().filter(r -> r.getNome().equalsIgnoreCase(nome))
                .collect(Collectors.toList());
    }

    private List<RespostaDTO> getRespostasByIdade(Integer idade) {
        return getRespostaDTOS()
                .stream().filter(r -> r.getIdade().compareTo(idade) == 0)
                .collect(Collectors.toList());
    }

    public List<RespostaDTO> getRespostaDTOS() {
        if (respostaDTOS == null) {
            respostaDTOS = Stream.of(RespostaDTO.builder()
                            .id(1L)
                            .nome("Bruno")
                            .idade(27)
                            .build(),
                    RespostaDTO.builder()
                            .id(2L)
                            .nome("João")
                            .idade(25)
                            .build(),
                    RespostaDTO.builder()
                            .id(3L)
                            .nome("Ledesma")
                            .idade(29)
                            .build())
                    .collect(Collectors.toList());
        }
        return respostaDTOS;
    }


}
