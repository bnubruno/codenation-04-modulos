package dev.codenation.modulo05.controller;

import dev.codenation.modulo05.dtos.CargoDTO;
import dev.codenation.modulo05.excecoes.NotFoundException;
import dev.codenation.modulo05.mappers.CargoMapper;
import dev.codenation.modulo05.services.CargoService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@AllArgsConstructor
@RequestMapping("/cargos")
public class CargoController {

    private CargoService cargoService;
    private CargoMapper cargoMapper;

    @GetMapping
    public List<CargoDTO> buscarTodos(@RequestParam(defaultValue = "0") Integer page,
                                      @RequestParam(defaultValue = "20") Integer size,
                                      @RequestParam(defaultValue = "id") String atributo,
                                      @RequestParam(defaultValue = "ASC") Sort.Direction direction) {
        return cargoMapper
                .paraCargoDTO(cargoService.buscarTodos(PageRequest.of(page, size, direction, atributo)).getContent());
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CargoDTO buscarPorId(@PathVariable("id") UUID id) throws NotFoundException {
        return cargoService.buscarPorId(id)
                .map(cargoMapper::paraCargoDTO)
                .orElseThrow(() -> new NotFoundException("Cargo não encontrado"));
    }

    @PostMapping
    public CargoDTO salvar(@RequestBody @Valid CargoDTO cargo) {
        return cargoMapper.paraCargoDTO(
                cargoService.salvar(cargoMapper.paraCargo(cargo, UUID.randomUUID()))
        );
    }


}
