package dev.codenation.modulo05.controller;

import dev.codenation.modulo05.dtos.RespostaDTO;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/hello")
public class HelloWorldController {

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<RespostaDTO> helloWorld() {
        return Arrays.asList(RespostaDTO.builder()
                        .nome("Bruno")
                        .idade(27)
                        .build(),
                RespostaDTO.builder()
                        .nome("João")
                        .idade(25)
                        .build(),
                RespostaDTO.builder()
                        .nome("Ledesma")
                        .idade(29)
                        .build());
    }

    @GetMapping(value = "/{parametro}")
    public String getPath(@PathVariable String parametro) {
        return parametro;
    }

    @GetMapping(value = "/param")
    public String getRequest(@RequestParam(value = "param") String parametro) {
        return parametro;
    }

    @PostMapping(value = "/{parametro}")
    public String postPath(@PathVariable String parametro) {
        return parametro;
    }

    @PostMapping(value = "/param")
    public String postRequest(@RequestParam(value = "param") String parametro) {
        return parametro;
    }

    @PostMapping
    public String postBody(@RequestBody String parametro) {
        return parametro;
    }

}
