package dev.codenation.modulo05.repositorios;

import dev.codenation.modulo05.entidades.Funcionario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface FuncionarioRepository extends CrudRepository<Funcionario, UUID> {
}
