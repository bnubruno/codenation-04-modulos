package dev.codenation.modulo05.repositorios;

import dev.codenation.modulo05.entidades.Pedido;
import dev.codenation.modulo05.entidades.PedidoId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PedidoRepository extends JpaRepository<Pedido, PedidoId> {

    List<Pedido> findByIdClienteNome(String nomeCliente);


}
