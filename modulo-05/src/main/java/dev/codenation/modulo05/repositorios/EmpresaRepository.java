package dev.codenation.modulo05.repositorios;

import dev.codenation.modulo05.entidades.Empresa;
import dev.codenation.modulo05.entidades.Funcionario;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface EmpresaRepository extends JpaRepository<Empresa, UUID> {

    @Query(value = "select e from Empresa e " +
            "join e.funcionarios f " +
            "where f.nome = :nomeFuncionario ")
    List<Empresa> buscarEmpresasPorNomeFuncionario(@Param("nomeFuncionario") String nomeFuncionario);

    List<Empresa> findByFuncionariosNome(String nome);

    List<Empresa> findByFuncionariosNomeAndFuncionariosMatricula(String nome, String matricula);

    List<Empresa> findByNome(String nome);

    List<Empresa> findByFuncionariosCargoDescricao(String descricaoCargo);

    List<Empresa> findByNomeAndFuncionariosCargoDescricao(String nomeEmpresa, String descricaoCargo);

}
