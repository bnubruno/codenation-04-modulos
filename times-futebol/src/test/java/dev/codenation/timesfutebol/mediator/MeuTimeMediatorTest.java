package dev.codenation.timesfutebol.mediator;

import dev.codenation.timesfutebol.exceptions.CapitaoNaoInformadoException;
import dev.codenation.timesfutebol.exceptions.IdentificadorUtilizadoException;
import dev.codenation.timesfutebol.exceptions.JogadorNaoEncontradoException;
import dev.codenation.timesfutebol.exceptions.TimeNaoEncontradoException;
import dev.codenation.timesfutebol.service.TimeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MeuTimeMediatorTest {

    @Autowired
    private MeuTimeMediator meuTimeMediator;

    @Autowired
    private TimeService timeService;

    @Test
    @Transactional
    public void deveIncluirTimeQueNaoExiste() {
        meuTimeMediator.incluirTime(1l, "Teste1", LocalDate.now(), "branco", "branco");
        assertEquals(new Long(1L), meuTimeMediator.buscarTimes().get(0));
    }

    @Test(expected = IdentificadorUtilizadoException.class)
    @Transactional
    public void deveLevantarExceptionQuandoOTimeJaExiste() {
        meuTimeMediator.incluirTime(1l, "Teste1", LocalDate.now(), "branco", "branco");
        meuTimeMediator.incluirTime(1l, "Teste1", LocalDate.now(), "branco", "branco");
    }

    @Test
    @Transactional
    public void deveIncluirJogadorNaoExistenteEmTimeJaExistente() {
        meuTimeMediator.incluirTime(1l, "Teste1", LocalDate.now(), "branco", "branco");
        meuTimeMediator.incluirJogador(1l, 1l, "Jogador", LocalDate.now(), 1, BigDecimal.TEN);
        assertEquals(new Long(1L), meuTimeMediator.buscarJogadoresDoTime(1L).get(0));
    }

    @Test(expected = TimeNaoEncontradoException.class)
    @Transactional
    public void aoAdicionarJogadorDeveLevantarExceptionQuandoTimeNaoExiste() {
        meuTimeMediator.incluirJogador(1l, 1l, "Jogador", LocalDate.now(), 1, BigDecimal.TEN);
    }

    @Test(expected = IdentificadorUtilizadoException.class)
    @Transactional
    public void aoAdicionarJogadorDeveLevantarExceptionQuandoJogadorJaExiste() {
        meuTimeMediator.incluirTime(1l, "Teste1", LocalDate.now(), "branco", "branco");
        meuTimeMediator.incluirJogador(1l, 1l, "Jogador", LocalDate.now(), 1, BigDecimal.TEN);
        meuTimeMediator.incluirJogador(1l, 1l, "Jogador", LocalDate.now(), 1, BigDecimal.TEN);
    }

    @Test
    @Transactional
    public void deveDefinirCapitao() {
        meuTimeMediator.incluirTime(1l, "Teste1", LocalDate.now(), "branco", "branco");
        meuTimeMediator.incluirJogador(1l, 1l, "Jogador", LocalDate.now(), 1, BigDecimal.TEN);
        meuTimeMediator.definirCapitao(1l);
        assertEquals(new Long(1L), meuTimeMediator.buscarCapitaoDoTime(1L));
    }

    @Test(expected = JogadorNaoEncontradoException.class)
    @Transactional
    public void deveLevantarExceptionAoDefinirJogadorInexistenteComoCapitao() {
        meuTimeMediator.definirCapitao(1l);
    }

    @Test(expected = TimeNaoEncontradoException.class)
    @Transactional
    public void deveLevantarExceptionAoBuscarCapitaoDeTimeInexistente() {
        meuTimeMediator.buscarCapitaoDoTime(1l);
    }

    @Test(expected = CapitaoNaoInformadoException.class)
    @Transactional
    public void deveLevantarExceptionCasoTimeNaoTenhaCapitao() {
        meuTimeMediator.incluirTime(1l, "Teste1", LocalDate.now(), "branco", "branco");
        meuTimeMediator.buscarCapitaoDoTime(1l);
    }

    @Test
    @Transactional
    public void deveBuscarNomeJogador() {
        meuTimeMediator.incluirTime(1l, "Teste1", LocalDate.now(), "branco", "branco");
        meuTimeMediator.incluirJogador(1l, 1l, "Jogador", LocalDate.now(), 1, BigDecimal.TEN);
        assertEquals("Jogador", meuTimeMediator.buscarNomeJogador(1L));
    }

    @Test(expected = JogadorNaoEncontradoException.class)
    @Transactional
    public void deveLevantarExceptionAoBuscarNomeDeJogadorQueNaoExiste() {
        meuTimeMediator.incluirTime(1l, "Teste1", LocalDate.now(), "branco", "branco");
        meuTimeMediator.buscarNomeJogador(1L);
    }

    @Test
    @Transactional
    public void deveBuscarNomeTime() {
        meuTimeMediator.incluirTime(1l, "Teste1", LocalDate.now(), "branco", "branco");
        assertEquals("Teste1", meuTimeMediator.buscarNomeTime(1L));
    }

    @Test(expected = TimeNaoEncontradoException.class)
    @Transactional
    public void deveLevantarExceptionQuandoBuscaNomeDeTimeInexistente() {
        meuTimeMediator.buscarNomeTime(1L);
    }

    @Test
    @Transactional
    public void deveBuscarJogadoresDoTime() {
        meuTimeMediator.incluirTime(1l, "Teste1", LocalDate.now(), "branco", "branco");
        meuTimeMediator.incluirJogador(2l, 1l, "Jogador", LocalDate.now(), 1, BigDecimal.TEN);
        meuTimeMediator.incluirJogador(3l, 1l, "Jogador", LocalDate.now(), 1, BigDecimal.TEN);
        List<Long> jogadoresTime = meuTimeMediator.buscarJogadoresDoTime(1L);
        assertTrue(jogadoresTime.contains(2L));
        assertTrue(jogadoresTime.contains(3L));
    }

    @Test(expected = TimeNaoEncontradoException.class)
    @Transactional
    public void deveLevantarExceptionAoBuscarJogadoresDeTimeInexistente() {
        List<Long> jogadoresTime = meuTimeMediator.buscarJogadoresDoTime(1L);
    }

    @Test
    @Transactional
    public void deveBuscarMelhorJogadorDoTime() {
        meuTimeMediator.incluirTime(1l, "Teste1", LocalDate.now(), "branco", "branco");
        meuTimeMediator.incluirJogador(2l, 1l, "Jogador", LocalDate.now(), 1, BigDecimal.TEN);
        meuTimeMediator.incluirJogador(3l, 1l, "Jogador2", LocalDate.now(), 2, BigDecimal.TEN);
        assertEquals(3L, meuTimeMediator.buscarMelhorJogadorDoTime(1L).longValue());
    }

    @Test(expected = TimeNaoEncontradoException.class)
    @Transactional
    public void deveLevantarExceptionAoBuscarMelhorJogadorDeTimeInexistente() {
        meuTimeMediator.buscarMelhorJogadorDoTime(1L);
    }

    @Test
    @Transactional
    public void deveBuscarJogadorMaisVelho() {
        meuTimeMediator.incluirTime(1l, "Teste1", LocalDate.now(), "branco", "branco");
        meuTimeMediator.incluirJogador(2l, 1l, "Jogador", LocalDate.now().minus(25, ChronoUnit.YEARS), 1, BigDecimal.TEN);
        meuTimeMediator.incluirJogador(3l, 1l, "Jogador2", LocalDate.now().minus(20, ChronoUnit.YEARS), 2, BigDecimal.TEN);
        assertEquals(2L, meuTimeMediator.buscarJogadorMaisVelho(1L).longValue());
    }

    @Test(expected = TimeNaoEncontradoException.class)
    @Transactional
    public void deveLevantarExceptionAoBuscarJogadorMaisVelhoDeTimeInexistente() {
        meuTimeMediator.buscarJogadorMaisVelho(1L);
    }

    @Test
    @Transactional
    public void deveBuscarTodosOsTimesExistentes() {
        meuTimeMediator.incluirTime(1l, "Teste1", LocalDate.now(), "branco", "branco");
        meuTimeMediator.incluirTime(2l, "Teste1", LocalDate.now(), "branco", "branco");
        meuTimeMediator.incluirTime(3l, "Teste1", LocalDate.now(), "branco", "branco");
        assertEquals(3L, meuTimeMediator.buscarTimes().size());
    }

    @Test
    @Transactional
    public void aoBuscarTimesDeveRetornarUmaListaVaziaQuandoNenhumTimeExiste() {
        assertEquals(0L, meuTimeMediator.buscarTimes().size());
    }

    @Test
    @Transactional
    public void deveBuscarJogadorComMaiorSalarioDoTime() {
        meuTimeMediator.incluirTime(1l, "Teste1", LocalDate.now(), "branco", "branco");
        meuTimeMediator.incluirJogador(2l, 1l, "Jogador", LocalDate.now().minus(25, ChronoUnit.YEARS), 1, new BigDecimal(10000));
        meuTimeMediator.incluirJogador(3l, 1l, "Jogador2", LocalDate.now().minus(20, ChronoUnit.YEARS), 2, new BigDecimal(20000));
        meuTimeMediator.incluirJogador(4l, 1l, "Jogador3", LocalDate.now().minus(20, ChronoUnit.YEARS), 2, new BigDecimal(30000));
        assertEquals(4L, meuTimeMediator.buscarJogadorMaiorSalario(1L).longValue());
    }

    @Test(expected = TimeNaoEncontradoException.class)
    @Transactional
    public void deveLevantarExceptionAoBuscarJogadorComMaiorSalarioDeTimeInexistente() {
        meuTimeMediator.buscarJogadorMaiorSalario(1L);
    }

    @Test
    @Transactional
    public void deveRetornarSalarioDoJogador() {
        meuTimeMediator.incluirTime(1l, "Teste1", LocalDate.now(), "branco", "branco");
        meuTimeMediator.incluirJogador(2l, 1l, "Jogador", LocalDate.now().minus(25, ChronoUnit.YEARS), 1, new BigDecimal(10000));
        meuTimeMediator.incluirJogador(3l, 1l, "Jogador2", LocalDate.now().minus(20, ChronoUnit.YEARS), 2, new BigDecimal(20000));
        meuTimeMediator.incluirJogador(4l, 1l, "Jogador3", LocalDate.now().minus(20, ChronoUnit.YEARS), 2, new BigDecimal(30000));
        assertEquals(new BigDecimal(10000), meuTimeMediator.buscarSalarioDoJogador(2L));
    }

    @Test(expected = JogadorNaoEncontradoException.class)
    @Transactional
    public void deveLevantarExceptionAoBuscarSalarioDeJogadorInexistente() {
        meuTimeMediator.buscarSalarioDoJogador(2L);
    }

    @Test
    @Transactional
    public void deveRetonarTopJogadores() {
        meuTimeMediator.incluirTime(1l, "Teste1", LocalDate.now(), "branco", "branco");
        meuTimeMediator.incluirJogador(2l, 1l, "Jogador", LocalDate.now().minus(25, ChronoUnit.YEARS), 1, new BigDecimal(10000));
        meuTimeMediator.incluirJogador(3l, 1l, "Jogador2", LocalDate.now().minus(20, ChronoUnit.YEARS), 2, new BigDecimal(20000));
        meuTimeMediator.incluirJogador(4l, 1l, "Jogador3", LocalDate.now().minus(20, ChronoUnit.YEARS), 3, new BigDecimal(30000));
        List<Long> jogadores = meuTimeMediator.buscarTopJogadores(2);
        assertEquals(2, jogadores.size());
        assertEquals(4L, jogadores.get(0).longValue());
        assertEquals(3L, jogadores.get(1).longValue());
    }

    @Test
    @Transactional
    public void aoBuscarTopJogadoresdeveRetonarListaVaziaAoBuscarQuandoNenhumJogadorExistir() {
        assertEquals(0, meuTimeMediator.buscarTopJogadores(2).size());
    }

    @Test
    @Transactional
    public void buscarNomes() {
        meuTimeMediator.incluirTime(1l, "Teste1", LocalDate.now(), "branco", "branco");
        meuTimeMediator.incluirJogador(2l, 1l, "Jogador", LocalDate.now().minus(25, ChronoUnit.YEARS), 1, new BigDecimal(10000));
        meuTimeMediator.incluirJogador(3l, 1l, "Jogador2", LocalDate.now().minus(20, ChronoUnit.YEARS), 2, new BigDecimal(20000));
        meuTimeMediator.incluirJogador(4l, 1l, "Jogador3", LocalDate.now().minus(20, ChronoUnit.YEARS), 3, new BigDecimal(30000));
        timeService.buscarNomes();
    }

}