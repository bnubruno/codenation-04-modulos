package dev.codenation.timesfutebol.repository;

import dev.codenation.timesfutebol.entity.Time;
import dev.codenation.timesfutebol.entity.TimeJogador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TimeRepository extends JpaRepository<Time, Long> {

    @Query("select new dev.codenation.timesfutebol.entity.TimeJogador(t.nome, j.nome) from Time as t " +
            "join t.jogadores as j ")
    List<TimeJogador> buscarTodosOsNome();

}
