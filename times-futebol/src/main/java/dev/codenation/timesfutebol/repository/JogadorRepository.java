package dev.codenation.timesfutebol.repository;

import dev.codenation.timesfutebol.entity.Jogador;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface JogadorRepository extends JpaRepository<Jogador, Long> {

    List<Jogador> findByTimeId(Long idTime);

    Optional<Jogador> findFirstByTimeIdOrderByHabilidadeDesc(Long idTime);

    Optional<Jogador> findFirstByTimeIdOrderByDataNascimentoAsc(Long idTime);

    Optional<Jogador> findFirstByTimeIdOrderBySalarioDesc(Long idTime);

}
