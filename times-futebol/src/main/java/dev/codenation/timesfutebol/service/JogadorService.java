package dev.codenation.timesfutebol.service;

import dev.codenation.timesfutebol.entity.Jogador;
import dev.codenation.timesfutebol.repository.JogadorRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class JogadorService {

    private JogadorRepository jogadorRepository;

    public Optional<Jogador> buscarPorId(Long id) {
        return jogadorRepository.findById(id);
    }

    public Jogador salvar(Jogador jogador) {
        return jogadorRepository.save(jogador);
    }

    public List<Jogador> buscarPorIdTime(Long idTime) {
        return jogadorRepository.findByTimeId(idTime);
    }

    public Optional<Jogador> buscarMelhorJogadorDoTime(Long idTime) {
        return jogadorRepository.findFirstByTimeIdOrderByHabilidadeDesc(idTime);
    }

    public Optional<Jogador> buscarJogadorMaisVelho(Long idTime) {
        return jogadorRepository.findFirstByTimeIdOrderByDataNascimentoAsc(idTime);
    }

    public Optional<Jogador> buscarJogadorMaiorSalario(Long idTime) {
        return jogadorRepository.findFirstByTimeIdOrderBySalarioDesc(idTime);
    }

    public List<Jogador> buscarTopJogadores(Integer top) {
        PageRequest request = PageRequest.of(0, top, Sort.Direction.DESC, "habilidade");
        return jogadorRepository.findAll(request).getContent();
    }
}
