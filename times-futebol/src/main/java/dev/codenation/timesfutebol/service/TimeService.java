package dev.codenation.timesfutebol.service;

import dev.codenation.timesfutebol.entity.Time;
import dev.codenation.timesfutebol.entity.TimeJogador;
import dev.codenation.timesfutebol.repository.TimeRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TimeService {

    private TimeRepository timeRepository;

    public Optional<Time> buscarPorId(Long id) {
        return timeRepository.findById(id);
    }

    public Time salvar(Time time) {
        return timeRepository.save(time);
    }

    public List<TimeJogador> buscarNomes() {
        return timeRepository.buscarTodosOsNome();
    }

    public List<Time> buscarTodos() {
        return timeRepository.findAll();
    }

    public List<Time> buscarCorCamisa(String text) {
        Time time = Time.builder()
                .corCamisaPrincipal(text)
                .corCamisaSecundaria(text)
                .nome(text)
                .build();
        return timeRepository.findAll(Example.of(time));
    }
}
