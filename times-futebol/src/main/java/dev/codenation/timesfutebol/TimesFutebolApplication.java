package dev.codenation.timesfutebol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TimesFutebolApplication {

	public static void main(String[] args) {
		SpringApplication.run(TimesFutebolApplication.class, args);
	}

}
