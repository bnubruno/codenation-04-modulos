package dev.codenation.timesfutebol.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Jogador {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jogador_seq")
    private Long id;

    @ManyToOne
    private Time time;

    private String nome;
    private LocalDate dataNascimento;
    private Integer habilidade;
    private BigDecimal salario;

}
