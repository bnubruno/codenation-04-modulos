package dev.codenation.timesfutebol.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Time {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "time_seq")
    private Long id;

    private String nome;
    private LocalDate dataCriacao;
    private String corCamisaPrincipal;
    private String corCamisaSecundaria;

    @OneToMany(mappedBy = "time")
    private List<Jogador> jogadores;

    @OneToOne
    private Jogador capitao;

    public Optional<Jogador> getCapitao() {
        return Optional.ofNullable(capitao);
    }
}
