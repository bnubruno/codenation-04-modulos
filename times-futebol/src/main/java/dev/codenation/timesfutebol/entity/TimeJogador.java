package dev.codenation.timesfutebol.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TimeJogador {

    private String nomeTime;
    private String nomeJogador;

}
