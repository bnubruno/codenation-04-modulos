package dev.codenation.timesfutebol.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TimeQuery {

    private String corCamisaPrincipal;
    private String corCamisaSecundaria;
}
