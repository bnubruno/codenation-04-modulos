package dev.codenation.timesfutebol.mediator;

import dev.codenation.timesfutebol.entity.Jogador;
import dev.codenation.timesfutebol.entity.Time;
import dev.codenation.timesfutebol.exceptions.CapitaoNaoInformadoException;
import dev.codenation.timesfutebol.exceptions.IdentificadorUtilizadoException;
import dev.codenation.timesfutebol.exceptions.JogadorNaoEncontradoException;
import dev.codenation.timesfutebol.exceptions.TimeNaoEncontradoException;
import dev.codenation.timesfutebol.service.JogadorService;
import dev.codenation.timesfutebol.service.TimeService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MeuTimeMediator implements MeuTimeInterface {

    private JogadorService jogadorService;
    private TimeService timeService;

    @Override
    public void incluirTime(Long id, String nome, LocalDate dataCriacao, String corUniformePrincipal, String corUniformeSecundario) {
        if (timeService.buscarPorId(id).isPresent()) {
            throw new IdentificadorUtilizadoException();
        }
        timeService.salvar(Time.builder()
                .id(id)
                .nome(nome)
                .dataCriacao(dataCriacao)
                .corCamisaSecundaria(corUniformeSecundario)
                .corCamisaPrincipal(corUniformePrincipal)
                .build());
    }

    @Override
    public void incluirJogador(Long id, Long idTime, String nome, LocalDate dataNascimento, Integer nivelHabilidade, BigDecimal salario) {
        Time time = timeService.buscarPorId(idTime).orElseThrow(() -> new TimeNaoEncontradoException());
        if (jogadorService.buscarPorId(id).isPresent()) {
            throw new IdentificadorUtilizadoException();
        }
        jogadorService.salvar(Jogador.builder()
                .id(id)
                .time(time)
                .nome(nome)
                .dataNascimento(dataNascimento)
                .habilidade(nivelHabilidade)
                .salario(salario)
                .build());
    }

    @Override
    public void definirCapitao(Long idJogador) {
        Jogador jogador = jogadorService.buscarPorId(idJogador).orElseThrow(() -> new JogadorNaoEncontradoException());
        Time time = timeService.buscarPorId(jogador.getTime().getId()).get();
        time.setCapitao(jogador);
        timeService.salvar(time);
    }

    @Override
    public Long buscarCapitaoDoTime(Long idTime) {
        Time time = timeService.buscarPorId(idTime)
                .orElseThrow(() -> new TimeNaoEncontradoException());
        return time.getCapitao()
                .map(Jogador::getId)
                .orElseThrow(() -> new CapitaoNaoInformadoException());
    }

    @Override
    public String buscarNomeJogador(Long idJogador) {
        return jogadorService.buscarPorId(idJogador)
                .map(Jogador::getNome)
                .orElseThrow(() -> new JogadorNaoEncontradoException());
    }

    @Override
    public String buscarNomeTime(Long idTime) {
        return timeService.buscarPorId(idTime)
                .map(Time::getNome)
                .orElseThrow(() -> new TimeNaoEncontradoException());
    }

    @Override
    public Long buscarJogadorMaiorSalario(Long idTime) {
        timeService.buscarPorId(idTime).orElseThrow(() -> new TimeNaoEncontradoException());
        return jogadorService.buscarJogadorMaiorSalario(idTime)
                .map(Jogador::getId)
                .orElseThrow(() -> new JogadorNaoEncontradoException());
    }

    @Override
    public BigDecimal buscarSalarioDoJogador(Long idJogador) {
        return jogadorService.buscarPorId(idJogador)
                .map(Jogador::getSalario)
                .orElseThrow(() -> new JogadorNaoEncontradoException());
    }

    @Override
    public List<Long> buscarJogadoresDoTime(Long idTime) {
        timeService.buscarPorId(idTime).orElseThrow(() -> new TimeNaoEncontradoException());
        return jogadorService.buscarPorIdTime(idTime)
                .stream()
                .map(Jogador::getId)
                .collect(Collectors.toList());
    }

    @Override
    public Long buscarMelhorJogadorDoTime(Long idTime) {
        timeService.buscarPorId(idTime).orElseThrow(() -> new TimeNaoEncontradoException());
        return jogadorService.buscarMelhorJogadorDoTime(idTime)
                .map(Jogador::getId)
                .orElseThrow(() -> new JogadorNaoEncontradoException());
    }

    @Override
    public Long buscarJogadorMaisVelho(Long idTime) {
        timeService.buscarPorId(idTime).orElseThrow(() -> new TimeNaoEncontradoException());
        return jogadorService.buscarJogadorMaisVelho(idTime)
                .map(Jogador::getId)
                .orElseThrow(() -> new JogadorNaoEncontradoException());
    }

    @Override
    public List<Long> buscarTimes() {
        return timeService.buscarTodos().stream()
                .map(Time::getId)
                .collect(Collectors.toList());
    }

    @Override
    public List<Long> buscarTopJogadores(Integer top) {
        return jogadorService.buscarTopJogadores(top)
                .stream()
                .map(Jogador::getId)
                .collect(Collectors.toList());
    }
}
