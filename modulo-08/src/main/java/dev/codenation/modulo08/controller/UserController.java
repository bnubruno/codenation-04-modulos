package dev.codenation.modulo08.controller;

import dev.codenation.modulo08.login.LoggedUser;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @GetMapping("/users")
    public String helloWorld() {
        return "Hello";
    }

    @GetMapping("/me")
    public String me() {
        LoggedUser loggedUser = (LoggedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return loggedUser.getUser().toString();
    }

}
