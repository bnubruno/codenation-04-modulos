package dev.codenation.servico;

import dev.codenation.modelo.NotaFiscal;

import java.time.LocalDate;
import java.util.Optional;

public class NotaFiscalService {

    public LocalDate buscarDataDeNascimentoCliente(NotaFiscal param) {
        return Optional.ofNullable(param)
                .map(nota -> nota.getCliente())
                .map(cliente -> cliente.getDataNascimento())
                .orElse(null);
    }

}
