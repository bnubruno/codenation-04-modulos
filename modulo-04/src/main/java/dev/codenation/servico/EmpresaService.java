package dev.codenation.servico;

import dev.codenation.modelo.Empresa;
import dev.codenation.modelo.Funcionario;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static java.util.Optional.ofNullable;

public class EmpresaService {

    public static final BigDecimal PERCENTUAL_IMPOSTO = new BigDecimal("1.4");

    public BigDecimal calcularFolhaPagamento(List<Funcionario> funcionarios, Function<BigDecimal, BigDecimal> tipoCalculo) {
        return tipoCalculo.apply(ofNullable(funcionarios)
                .orElse(new ArrayList<>())
                .stream()
                .map(Funcionario::getSalario)
                .reduce(BigDecimal.ZERO, BigDecimal::add));
    }

    public BigDecimal getSalario(Funcionario funcionario) {
        return funcionario.getSalario();
    }

}
