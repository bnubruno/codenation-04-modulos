package dev.codenation.servico;

public enum NotaFiscalSingleton {

    INSTANCE(new NotaFiscalService());

    private NotaFiscalService notaFiscalService;

    NotaFiscalSingleton(NotaFiscalService notaFiscalService) {
        this.notaFiscalService = notaFiscalService;
    }

    public NotaFiscalService get() {
        return notaFiscalService;
    }
}
