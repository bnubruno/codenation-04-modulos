package dev.codenation.modelo;

import java.util.function.Function;

public enum TipoVolume {

    AUMENTAR((volumeAtual) -> volumeAtual + 1),
    DIMINUIR((volumeAtual) -> volumeAtual - 1);

    private Function<Integer, Integer> function;

    TipoVolume(Function<Integer, Integer> function) {
        this.function = function;
    }

    public Function<Integer, Integer> getFunction() {
        return function;
    }
}
