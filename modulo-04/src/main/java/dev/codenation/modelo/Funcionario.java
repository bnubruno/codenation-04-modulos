package dev.codenation.modelo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Funcionario extends Pessoa {

    private Long codigoFuncionario;
    private BigDecimal salario;
}
