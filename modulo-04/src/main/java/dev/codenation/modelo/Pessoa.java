package dev.codenation.modelo;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
public class Pessoa {

    private Long id;
    private String nome;
    private LocalDate dataNascimento;

    public void setNome(String nome) {
        this.nome = nome;
    }
}
