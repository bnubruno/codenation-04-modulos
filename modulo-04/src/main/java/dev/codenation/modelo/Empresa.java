package dev.codenation.modelo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
public class Empresa {

    private String razaoSocial;
    private List<Funcionario> funcionarios = new ArrayList<>();
    private Pessoa dono;

    public static BigDecimal fazQualquerCoisa(BigDecimal a, BigDecimal b) {
        return null;
    }

}
