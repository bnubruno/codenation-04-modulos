package dev.codenation.modelo;

import lombok.Data;


@Data
public class Televisao {

    private Integer volume = 0;
    private Integer canal = 0;

}
