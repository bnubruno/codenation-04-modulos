package dev.codenation.modelo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class NotaFiscal {

    private Long id;
    private Empresa empresa;
    private Pessoa cliente;
    private LocalDateTime data;

}
