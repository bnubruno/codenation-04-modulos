package dev.codenation.modelo;

public class ControleRemoto {

    private final Televisao televisao;

    public ControleRemoto(Televisao televisao) {
        this.televisao = televisao;
    }

    public void setaVolume(TipoVolume tipoVolume) {
        this.televisao.setVolume(tipoVolume.getFunction().apply(this.televisao.getVolume()));
    }

    public void diminuirVolume() {
        this.televisao.setVolume(TipoVolume.AUMENTAR.getFunction().apply(this.televisao.getVolume()));
    }

    public void aumentarNumeroCanal() {
        this.televisao.setCanal(this.televisao.getCanal() + 1);
    }

    public void diminuirNumeroCanal() {
        this.televisao.setCanal(this.televisao.getCanal() - 1);
    }

    public void setarCanal(Integer canal) {
        this.televisao.setCanal(canal);
    }

    public Integer consultarCanal() {
        return this.televisao.getCanal();
    }
}
