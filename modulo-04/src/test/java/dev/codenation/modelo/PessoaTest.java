package dev.codenation.modelo;

import dev.codenation.modelo.Pessoa;
import org.junit.Test;

import java.util.HashSet;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class PessoaTest {

    @Test
    public void testarPessoaDuplicada() {
        HashSet<Pessoa> hashSet = new HashSet<>();

        Pessoa pessoa01 = new Pessoa();
        pessoa01.setId(1L);
        pessoa01.setNome("Bruno");

        Pessoa pessoa02 = new Pessoa();
        pessoa02.setId(1L);
        pessoa02.setNome("João");

        hashSet.add(pessoa01);
        hashSet.add(pessoa02);

//        assertThat(pessoa01, equalTo(pessoa02));
        assertThat(hashSet.size(), equalTo(2));
    }

}
