package dev.codenation.modelo;

import dev.codenation.modelo.Funcionario;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class FuncionarioTest {

    @Test
    public void testeFuncionarioDuplicado() {
        Funcionario funcionario01 = new Funcionario();
        Funcionario funcionario02 = new Funcionario();

        funcionario01.setCodigoFuncionario(1l);
        funcionario02.setCodigoFuncionario(1l);

        assertThat(funcionario01, equalTo(funcionario02));
    }

}
