package dev.codenation.servico;

import dev.codenation.modelo.Funcionario;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

public class EmpresaServiceTest {

    private EmpresaService empresaService = new EmpresaService();

    @Test
    public void quandoCalculaFolhaPagamentoNula() {
        BigDecimal result = empresaService.calcularFolhaPagamento(null,
                x -> x.multiply(new BigDecimal("2")));

        MatcherAssert.assertThat(result, Matchers.comparesEqualTo(BigDecimal.ZERO));
    }

    @Test
    public void quandoCalculaFolhaPagamentoSemFuncionarios() {
        BigDecimal result = empresaService.calcularFolhaPagamento(new ArrayList<>(),
                x -> x.multiply(new BigDecimal("1.4")));

        assertThat(result, Matchers.comparesEqualTo(BigDecimal.ZERO));
    }

    @Test
    public void quandoCalculaFolhaPagamentoComFuncionarios() {
        List<Funcionario> listaDeFuncionarios = Arrays.asList(makeFuncionario(new BigDecimal("1000")),
                makeFuncionario(new BigDecimal("2000")),
                makeFuncionario(new BigDecimal("3000")));

        BigDecimal result = empresaService.calcularFolhaPagamento(listaDeFuncionarios,
                x -> x.multiply(new BigDecimal("1.4")));

        assertThat(listaDeFuncionarios, hasSize(3));
        assertThat(result, Matchers.comparesEqualTo(new BigDecimal("8400")));
    }

    private Funcionario makeFuncionario(BigDecimal salario) {
        Funcionario funcionario = new Funcionario();
        funcionario.setSalario(salario);
        return funcionario;
    }

}
