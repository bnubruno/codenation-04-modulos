package dev.codenation.servico;

import dev.codenation.modelo.Funcionario;
import dev.codenation.modelo.NotaFiscal;
import dev.codenation.modelo.Pessoa;
import dev.codenation.modelo.Safe;
import dev.codenation.servico.NotaFiscalSingleton;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;

import static dev.codenation.modelo.Safe.safeConverter;
import static java.util.Optional.of;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class NotaFiscalServiceTest {

    private static final LocalDate DATA_NASCIMENTO = LocalDate.of(1990, 1, 1);

    @Test
    public void buscarDataNascimentoQuandoParametroEhNulo() {
        LocalDate result = NotaFiscalSingleton.INSTANCE.get().buscarDataDeNascimentoCliente(null);

        assertThat(result, nullValue());
    }

    @Test
    public void buscarDataNascimentoQuandoParametroEhNotaSemCliente() {
        NotaFiscal notafiscal = new NotaFiscal();

        LocalDate result = NotaFiscalSingleton.INSTANCE.get().buscarDataDeNascimentoCliente(notafiscal);

        assertThat(result, nullValue());
    }

    @Test
    public void buscarDataNascimentoQuandoParametroEhNotaComCliente() {
        NotaFiscal notafiscal = new NotaFiscal();
        Pessoa pessoa = new Pessoa();
        pessoa.setDataNascimento(DATA_NASCIMENTO);
        notafiscal.setCliente(pessoa);

        LocalDate result = NotaFiscalSingleton.INSTANCE.get().buscarDataDeNascimentoCliente(notafiscal);

        assertThat(result, equalTo(DATA_NASCIMENTO));
    }

    @Test
    public void abc() {
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("7");

        Funcionario funcionario = new Funcionario();
        funcionario.setNome("7");

        pessoa.setNome(pessoa.getNome().substring(0, 10));
        funcionario.setNome(funcionario.getNome().substring(0, 7));

        truncate(pessoa, Pessoa::getNome, Pessoa::setNome, 10);
        truncate(funcionario, Funcionario::getNome, Funcionario::setNome, 7);

        assertThat(pessoa.getNome().length(), Matchers.equalTo(10));
        assertThat(funcionario.getNome().length(), Matchers.equalTo(7));
    }

    private <I> void truncate(I objeto, Function<I, String> getMethod, BiConsumer<I, String> setMethod, Integer qtdTruncar) {
        String str = getMethod.apply(objeto);
        int min = Math.min(str.length(), qtdTruncar);
        setMethod.accept(objeto, str.substring(0, min));
    }


}
