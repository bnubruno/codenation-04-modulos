package dev.codenation.servico;

import dev.codenation.modelo.ControleRemoto;
import dev.codenation.modelo.Televisao;
import dev.codenation.modelo.TipoVolume;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Test;

public class ControleRemotoTest {

    @Test
    public void quandoSetaCanal() {
        ControleRemoto controleRemoto = new ControleRemoto(new Televisao());

        controleRemoto.setarCanal(10);

        MatcherAssert.assertThat(controleRemoto.consultarCanal(), Matchers.equalTo(10));
    }

    @Test
    public void quandoAumentaVolume() {
        Televisao televisao = new Televisao();
        televisao.setVolume(30);
        ControleRemoto controleRemoto = new ControleRemoto(televisao);

        controleRemoto.setaVolume(TipoVolume.AUMENTAR);
        controleRemoto.setaVolume(TipoVolume.DIMINUIR);

        MatcherAssert.assertThat(televisao.getVolume(), Matchers.equalTo(31));
    }

    @Test
    public void quandoDiminuiVolume() {
        Televisao televisao = new Televisao();
        televisao.setVolume(30);
        ControleRemoto controleRemoto = new ControleRemoto(televisao);

        controleRemoto.diminuirVolume();

        MatcherAssert.assertThat(televisao.getVolume(), Matchers.equalTo(29));
    }

    @Test
    public void quandoAumentaCanal() {
        Televisao televisao = new Televisao();
        televisao.setCanal(55);
        ControleRemoto controleRemoto = new ControleRemoto(televisao);

        controleRemoto.aumentarNumeroCanal();

        MatcherAssert.assertThat(televisao.getCanal(), Matchers.equalTo(56));
    }

    @Test
    public void quandoDiminuirCanal() {
        Televisao televisao = new Televisao();
        televisao.setCanal(55);
        ControleRemoto controleRemoto = new ControleRemoto(televisao);

        controleRemoto.diminuirNumeroCanal();

        MatcherAssert.assertThat(televisao.getCanal(), Matchers.equalTo(54));
    }

}
